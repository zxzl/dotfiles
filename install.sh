# Xcode command line tool
xcode-select --install

# Install homebrew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# Git setting
git config --global core.editor "vim"

# Install zsh
brew install zsh zsh-completions
# zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
# zsh-autosuggestions
git clone git://github.com/zsh-users/zsh-autosuggestions $ZSH_CUSTOM/plugins/zsh-autosuggestion

###
# DevTools
###

# Install tmux
brew install tmux

# Install fzf
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install
source ~/.zshrc

# Improve vim
git clone --depth=1 https://github.com/amix/vimrc.git ~/.vim_runtime
sh ~/.vim_runtime/install_awesome_vimrc.sh

# Install bat (cat alternative, https://github.com/sharkdp/bat)
brew install bat

# Install watch
brew install watch


###
# JavaScript
###
# Install nvm and latest LTS version
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
nvm install --lts
nvm alias default node

# Install Yarn
brew install yarn

###
# Python
###
# http://hellogohn.com/post_one282
brew install pyenv
CFLAGS="-I$(brew --prefix openssl)/include -I$(xcrun --show-sdk-path)/usr/include" \
LDFLAGS="-L$(brew --prefix openssl)/lib" \
pyenv install -v 3.6.8

###
# Java
###
brew install jenv
brew cask install java
brew cask install caskroom/versions/java8

# Install various GUI tools
brew cask install docker
brew cask install iina
brew cask install iterm2
brew cask install postman
brew cask install google-chrome
brew cask install visual-studio-code
brew cask install insomnia
brew cask install github
brew cask install bettertouchtool
brew cask install notion
brew cask install slack
brew cask install mattermost

# Install IntelliJ tools
brew cask install intellij-idea
brew cask install pycharm
